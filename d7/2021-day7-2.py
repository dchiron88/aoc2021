# -*-coding:Latin-1 -*
import os

def calcDay7_1(l):
  i=min(l)
  M=max(l)
  pos=0
  fuel=M*len(l)
  while i<=M:
    #print("-----for position",i)
    diff=calcdiff(l,i)
    #print(diff)
    if diff<fuel:
      fuel=diff
      pos=i
    i+=1
  return pos,fuel

def calcdiff(l,nb):
  res=0
  for elt in l:
    res+=abs(elt-nb)
  return res

def calcDay7_2(l):
  i=min(l)
  M=max(l)
  pos=0
  fuel=0
  while i<=M:
    #print("-----for position",i)
    diff=calcdiff2(l,i)
    #print(diff)
    if diff<fuel or fuel==0:
      fuel=diff
      pos=i
    i+=1
  return pos,fuel

def calcdiff2(l,nb):
  res=0
  for elt in l:
    #res+=calculfuel(elt,nb)
    c=abs(elt-nb)
    res+=int(c*(c+1)/2)
  return res

"""def calculfuel(a,b):
  c=0
  i=1
  while i<=abs(a-b):
    c+=i
    i+=1
  return c"""

def main():
    f=open('input.txt','r')
    #f=open('test.txt','r')
    inp=f.read()
    liste=inp.split(",") #récupération de la liste dans le fichier
    liste=[int(i) for i in liste] #conversion en int
    print("result part 1:",calcDay7_1(liste))
    print("result part 2:",calcDay7_2(liste))
    f.close()

main()
