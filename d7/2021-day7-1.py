# -*-coding:Latin-1 -*
import os

def calcDay7_1(l):
  i=min(l)
  M=max(l)
  pos=0
  fuel=M*len(l)
  while i<=M:
    #print("-----for position",i)
    diff=calcdiff(l,i)
    #print(diff)
    if diff<fuel:
      fuel=diff
      pos=i
    i+=1
  return pos,fuel

def calcdiff(l,nb):
  res=0
  for elt in l:
    res+=abs(elt-nb)
  return res

def calculate_median(l):
    l = sorted(l)
    l_len = len(l)
    if l_len < 1:
        return None
    if l_len % 2 == 0 :
        #print("pair",int(l_len/2-1))
        return ( l[int(l_len/2-1)] + l[int(l_len/2)] ) / 2.0
    else:
        #print("impair",(l_len-1)/2)
        return l[int((l_len-1)/2)]

def main():
    f=open('input.txt','r')
    #f=open('test.txt','r')
    inp=f.read()
    liste=inp.split(",") #récupération de la liste dans le fichier
    liste=[int(i) for i in liste] #conversion en int
    print("result:",calcDay7_1(liste))
    print("by median:",int(calculate_median(liste)))
    f.close()

main()
