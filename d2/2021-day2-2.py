# -*-coding:Latin-1 -*
import os

def getTuple(t):
    a=t.split(" ")
    return (a[0],int(a[1]))

def calcDay2_2(l):
    res=0
    ho=0 #horizontal position
    de=0 #depth
    aim=0
    i=0
    while i<len(l):
        (di,nb)=getTuple(l[i]) #di for direction and nb for value
        if di=="forward":
            ho+=nb
            de+=aim*nb
        elif di=="down":
            aim+=nb
        elif di=="up":
            aim-=nb
        else:
            print(di, "does not exist")
        i+=1
    res=ho*de
    return res

def calcDay2(l):
    res=0
    ho=0 #horizontal position
    de=0 #depth
    i=0
    while i<len(l):
        (di,nb)=getTuple(l[i]) #di for direction and nb for value
        if di=="forward":
            ho+=nb
        elif di=="down":
            de+=nb
        elif di=="up":
            de-=nb
        else:
            print(di, "does not exist")
        i+=1
    res=ho*de
    return res

def main():
    f=open('input.txt','r')
    #f=open('test.txt','r')
    inp=f.read()
    liste=inp.split("\n") #récupération de la liste dans le fichier
    #liste=[int(i) for i in liste] #conversion en int
    print("result part 1:",calcDay2(liste))
    print("result part 2:",calcDay2_2(liste))
    f.close()

main()
