# -*-coding:Latin-1 -*
import os
import numpy as np
import re

def calcMax(li):
    m=0
    for elt in li:
        a=re.split(' -> |,',elt)
        a=[int(i) for i in a]
        ma=max(a)
        if m<ma:
            m=ma
    return m

def calcMaxX(li):
    mx=0
    for elt in li:
        a=elt.split('->')
        b=a[0].split(',')
        c=a[1].split(',')
        mt=max(int(b[0]),int(c[0]))
        if mx<mt:
            mx=mt
    return mx

def calcMaxY(li):
    my=0
    for elt in li:
        a=elt.split(' -> ')
        b=a[0].split(',')
        c=a[1].split(',')
        mt=max(int(b[1]),int(c[1]))
        if my<mt:
            my=mt
    return my

def printArray(a):
    for i in a:
        for j in i:
            print(j, end ="")
        print()
    return 0

def fillArray(ar,l):
    a=l.split(' -> ')
    a1=a[0].split(',')
    a2=a[1].split(',')
    x1=int(a1[0])
    y1=int(a1[1])
    x2=int(a2[0])
    y2=int(a2[1])
    #print(x1,y1,x2,y2)
    if x1==x2:
        i=min(y1,y2)
        while i<=max(y1,y2):
            ar[i,x1]+=1
            i+=1
    elif y1==y2:
        i=min(x1,x2)
        while i<=max(x1,x2):
            ar[y1,i]+=1
            i+=1
    else:
        #print("the line",l,"is not taken into account")
        return 0
    return 0

def countOverlap(a):
    nb=0
    for i,j in np.ndindex(a.shape):
        if a[i,j]>1:
            #print(a[i,j])
            nb+=1
    return nb

def day5_1(l):
    M=calcMax(l)
    #print("max =",M)
    array=np.full((M+1,M+1),0)
    #printArray(array)
    for elt in l:
        fillArray(array,elt)
    #printArray(array)
    return countOverlap(array)

def main():
    f=open('input.txt','r')
    #f=open('test.txt','r')
    inp=f.read()
    liste=inp.split("\n") #récupération de la liste dans le fichier
    #liste=[int(i) for i in liste] #conversion en int
    print("result:",day5_1(liste))
    #createMatrix()
    f.close()

main()
