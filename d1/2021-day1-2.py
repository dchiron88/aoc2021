# -*-coding:Latin-1 -*
import os

def calcInc(l):
    i=1
    n=0
    while i<len(l):
        if l[i]>l[i-1]:
            n+=1
        i+=1
    return n

#Three-measurement sliding window given a liste l and a position i
def calcTMSW(l,i):
    n=0
    j=0
    #if i+3>=len(l):
    #    return 0
    while j<3:
        n+=l[i+j]
        j+=1
    return n

def calcInc2(l):
    i=1
    n=0
    while i<=len(l)-3:
        #print(i,calcTMSW(l,i),calcTMSW(l,i-1))
        if calcTMSW(l,i)>calcTMSW(l,i-1):
            #print("increase")
            n+=1
        i+=1
    return n

def main():
    f=open('input.txt','r')
    #f=open('test.txt','r')
    inp=f.read()
    liste=inp.split("\n") #récupération de la liste dans le fichier
    liste=[int(i) for i in liste] #conversion en int
    print("result:",calcInc2(liste))
    f.close()

main()
