# -*-coding:Latin-1 -*
import os

def calcInc(l):
    i=1
    n=0
    while i<len(l):
        if l[i]>l[i-1]:
            n+=1
        i+=1
    return n
    
def main():
    f=open('input.txt','r')
    #f=open('test.txt','r')
    inp=f.read()
    liste=inp.split("\n") #récupération de la liste dans le fichier
    liste=[int(i) for i in liste] #conversion en int
    print(calcInc(liste))
    f.close()

main()
