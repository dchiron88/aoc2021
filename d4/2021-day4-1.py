# -*-coding:Latin-1 -*
import os
import numpy as np
import re

"""def calcMax(li):
    m=0
    for elt in li:
        a=re.split(' -> |,',elt)
        a=[int(i) for i in a]
        ma=max(a)
        if m<ma:
            m=ma
    return m

def calcMaxX(li):
    mx=0
    for elt in li:
        a=elt.split('->')
        b=a[0].split(',')
        c=a[1].split(',')
        mt=max(int(b[0]),int(c[0]))
        if mx<mt:
            mx=mt
    return mx

def calcMaxY(li):
    my=0
    for elt in li:
        a=elt.split(' -> ')
        b=a[0].split(',')
        c=a[1].split(',')
        mt=max(int(b[1]),int(c[1]))
        if my<mt:
            my=mt
    return my"""

def printArray(a):
    for i in a:
        for j in i:
            print(j, end =" ")
        print()
    return 0

"""def fillArray(ar,l):
    a=l.split(' -> ')
    a1=a[0].split(',')
    a2=a[1].split(',')
    x1=int(a1[0])
    y1=int(a1[1])
    x2=int(a2[0])
    y2=int(a2[1])
    #print(x1,y1,x2,y2)
    if x1==x2:
        i=min(y1,y2)
        while i<=max(y1,y2):
            ar[i,x1]+=1
            i+=1
    elif y1==y2:
        i=min(x1,x2)
        while i<=max(x1,x2):
            ar[y1,i]+=1
            i+=1
    else:
        #print("the line",l,"is not taken into account")
        return 0
    return 0

def countOverlap(a):
    nb=0
    for i,j in np.ndindex(a.shape):
        if a[i,j]>1:
            #print(a[i,j])
            nb+=1
    return nb

def day5_1(l):
    M=calcMax(l)
    #print("max =",M)
    array=np.full((M+1,M+1),0)
    #printArray(array)
    for elt in l:
        fillArray(array,elt)
    #printArray(array)
    return countOverlap(array)"""

def createArray(text):
    #print(text,"\n")
    a=[]
    li=text.split("\n")
    #print(li)
    i=0
    while i<len(li):
        nb=li[i].split()
        #nb=re.split(' |  ',li[i])
        nb=[int(i) for i in nb]
        #print("nb =",nb)
        j=0
        temp=[]
        while j<len(nb):
            #print(li[i][j])
            temp.append(nb[j])
            j+=1
        a.append(temp)
        i+=1
    return a

def createArray0(nb):
    '''at=[[0,0,0,0,0] for count in range(5)]
    a0=[at for count in range(nb)]'''
    a0=[[[0,0,0,0,0] for count in range(5)] for count in range(nb)]
    return a0

def winner(c): #c for checker
    i=0
    while i<len(c):
        j=0
        v=[0]*len(c[i])
        while j<len(c[i]):
            k=0
            h=0
            while k<len(c[i][j]):
                if c[i][j][k]==1:
                    h+=1
                    v[k]+=1
                k+=1
            if h==len(c[i][j]):
                return True,i
            j+=1
        for elt in v:
            if elt==5:
                return True,i
        i+=1
    return False,-1

def fillChecker(c,b,nb): #c for checker, b for board
    i=0
    while i<len(b):
        #print("i=",i,"**********")
        #printArray(c[i])
        j=0
        while j<len(b[i]):
            #print("*j=",j)
            k=0
            while k<len(b[i][j]):
                #print("**k=",k)
                if b[i][j][k]==nb:
                    #printArray(c[i])
                    #print(i,j,k,c[i][j][k])
                    c[i][j][k]=1
                    #c[i,j,k]=1
                    #printArray(c[i])
                k+=1
            j+=1
        i+=1
    return 0

def unmarkedSum(c,b,index):
    s=0
    i=0
    while i<len(c[index]):
        j=0
        while j<len(c[index][i]):
            if c[index][i][j]==0:
                s+=b[index][i][j]
            j+=1
        i+=1
    return s

def day4_1(l):
    draw=l[0].split(",")
    draw=[int(i) for i in draw]
    #print("draw\n",draw,sep="")
    boards=[]
    i=1
    while i<len(l):
        boards.append(createArray(l[i]))
        i+=1
    '''print("boards")
    for elt in boards:
        printArray(elt)
        print()'''
    checker=createArray0(len(boards))
    '''print("checker")
    for elt in checker:
        printArray(elt)
        print()'''
    #fillChecker(checker,boards,draw[0])
    w=False
    d_index=0
    while w==False:
        fillChecker(checker,boards,draw[d_index])
        """print(draw[d_index])
        for elt in checker:
            printArray(elt)"""
        d_index+=1
        w,nbBoard=winner(checker)
        #w=True
    
    return draw[d_index-1]*unmarkedSum(checker,boards,nbBoard)

def main():
    f=open('input.txt','r')
    #f=open('test.txt','r')
    inp=f.read()
    liste=inp.split("\n\n") #récupération de la liste dans le fichier
    #liste=[int(i) for i in liste] #conversion en int
    print("result:",day4_1(liste))
    #createMatrix()
    f.close()

main()
