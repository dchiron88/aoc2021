# -*-coding:Latin-1 -*
import os
import numpy as np
import re

def printArray(a):
    for i in a:
        for j in i:
            print(j, end =" ")
        print()
    return 0

def createArray(text):
    #print(text,"\n")
    a=[]
    li=text.split("\n")
    #print(li)
    i=0
    while i<len(li):
        nb=li[i].split()
        #nb=re.split(' |  ',li[i])
        nb=[int(i) for i in nb]
        #print("nb =",nb)
        j=0
        temp=[]
        while j<len(nb):
            #print(li[i][j])
            temp.append(nb[j])
            j+=1
        a.append(temp)
        i+=1
    return a

def createArray0(nb):
    '''at=[[0,0,0,0,0] for count in range(5)]
    a0=[at for count in range(nb)]'''
    a0=[[[0,0,0,0,0] for count in range(5)] for count in range(nb)]
    return a0

def winner(c): #c for checker
    i=0
    while i<len(c):
        j=0
        v=[0]*len(c[i])
        while j<len(c[i]):
            k=0
            h=0
            while k<len(c[i][j]):
                if c[i][j][k]==1:
                    h+=1
                    v[k]+=1
                k+=1
            if h==len(c[i][j]):
                return True,i
            j+=1
        for elt in v:
            if elt==5:
                return True,i
        i+=1
    return False,-1

def listWinner(c):
    l=[]
    i=0
    while i<len(c):
        j=0
        v=[0]*len(c[i])
        while j<len(c[i]):
            k=0
            h=0
            while k<len(c[i][j]):
                if c[i][j][k]==1:
                    h+=1
                    v[k]+=1
                k+=1
            if h==len(c[i][j]):
                l.append(i)
            j+=1
        for elt in v:
            if elt==5:
                l.append(i)
        i+=1
    return list(dict.fromkeys(l))

def fillChecker(c,b,nb): #c for checker, b for board
    i=0
    while i<len(b):
        #print("i=",i,"**********")
        #printArray(c[i])
        j=0
        while j<len(b[i]):
            #print("*j=",j)
            k=0
            while k<len(b[i][j]):
                #print("**k=",k)
                if b[i][j][k]==nb:
                    #printArray(c[i])
                    #print(i,j,k,c[i][j][k])
                    c[i][j][k]=1
                    #c[i,j,k]=1
                    #printArray(c[i])
                k+=1
            j+=1
        i+=1
    return 0

def unmarkedSum(c,b,index):
    s=0
    i=0
    while i<len(c[index]):
        j=0
        while j<len(c[index][i]):
            if c[index][i][j]==0:
                s+=b[index][i][j]
            j+=1
        i+=1
    return s

def day4_1(l):
    draw=l[0].split(",")
    draw=[int(i) for i in draw]
    #print("draw\n",draw,sep="")
    boards=[]
    i=1
    while i<len(l):
        boards.append(createArray(l[i]))
        i+=1
    '''print("boards")
    for elt in boards:
        printArray(elt)
        print()'''
    checker=createArray0(len(boards))
    '''print("checker")
    for elt in checker:
        printArray(elt)
        print()'''
    #fillChecker(checker,boards,draw[0])
    w=False
    d_index=0
    while w==False:
        fillChecker(checker,boards,draw[d_index])
        """print(draw[d_index])
        for elt in checker:
            printArray(elt)"""
        d_index+=1
        w,nbBoard=winner(checker)
        #w=True
    
    return draw[d_index-1]*unmarkedSum(checker,boards,nbBoard)

def day4_2(l):
    draw=l[0].split(",")
    draw=[int(i) for i in draw]
    #print("draw\n",draw,sep="")
    boards=[]
    i=1
    while i<len(l):
        boards.append(createArray(l[i]))
        i+=1
    checker=createArray0(len(boards))
    nbWinner=0
    w=False
    d_index=0
    while nbWinner<len(boards):
        #print("on tire le num�ro ",draw[d_index])
        fillChecker(checker,boards,draw[d_index])
        w,nbBoard=winner(checker)
        """if w:
            nbWinner+=1"""
        #print(listWinner(checker))
        nbWinner=len(listWinner(checker))
        if len(listWinner(checker))==len(boards)-1:
            lt=[]
            plop=0
            while plop<len(boards):
                lt.append(plop)
                plop+=1
            for elt in listWinner(checker):
                lt.remove(elt)
            lastWinnerIndex=lt[0]
        d_index+=1
        #w,nbBoard=winner(checker)
    #print(draw[d_index-1],unmarkedSum(checker,boards,lastWinnerIndex))
    return draw[d_index-1]*unmarkedSum(checker,boards,lastWinnerIndex)

def main():
    #f=open('input.txt','r')
    f=open('test.txt','r')
    inp=f.read()
    liste=inp.split("\n\n") #r�cup�ration de la liste dans le fichier
    #liste=[int(i) for i in liste] #conversion en int
    print("result:",day4_2(liste))
    #createMatrix()
    f.close()

main()
