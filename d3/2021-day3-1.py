# -*-coding:Latin-1 -*
import os

def calcDay3_1(l):
    l1=countOnes(l)
    i=0
    gammabit=""
    epsilonbit=""
    while i<len(l1):
        #print("bit nb",i+1)
        nb0=len(l)-l1[i]
        nb1=l1[i]
        """print("nb of 0:",nb0)
        print("nb of 1:",nb1)"""
        if nb0>nb1:
            gammabit+="0"
            epsilonbit+="1"
        elif nb0<nb1:
            gammabit+="1"
            epsilonbit+="0"
        else:
            print("What do I choose?")
        i+=1
    #print("gamma",gammabit,"| epsilon",epsilonbit)
    gamma=int("0b"+gammabit,base=2)
    epsilon=int("0b"+epsilonbit,base=2)
    #print("gamma",gamma,"| epsilon",epsilon)
    return gamma*epsilon

def countOnes(l):
    #l1=[0,0,0,0,0]
    l1=[]
    j=0
    while j<len(l[0]):
        l1.append(0)
        j+=1
    i=0
    while i<len(l):
        incrOnes(l[i],l1)
        i+=1
    return l1

#Function to increment the nb of occurences of ones, all is stored in a list
#t for input text (string of a binary nb) and l for temporary list
def incrOnes(t,l1):
    i=0
    while i<len(t):
        if t[i]=="1":
            l1[i]+=1
        i+=1
    return l1

def main():
    f=open('input.txt','r')
    #f=open('test.txt','r')
    inp=f.read()
    liste=inp.split("\n") #récupération de la liste dans le fichier
    #liste=[int(i) for i in liste] #conversion en int
    print("result:",calcDay3_1(liste))
    f.close()

main()
