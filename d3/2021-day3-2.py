# -*-coding:Latin-1 -*
import os
import copy

def countOnesPos(l,pos):
    i=0
    n=0
    while i<len(l):
        if l[i][pos]=="1":
            n+=1
        i+=1
    return n

def erase(l,pos,bit): #bit can be "0" or "1"
    i=0
    while i<len(l):
        if l[i][pos]==bit:
            l.pop(i)
        else:
            i+=1
    return l

def calcDay3_2(l):
    #oxygen generator rating
    l0=copy.deepcopy(l)
    l1=copy.deepcopy(l)
    pos=0
    initlen=len(l0)
    while len(l0)>1:
        c=countOnesPos(l0,pos)
        if c>len(l0)-c:
            l0=erase(l0,pos,"0")
        elif c<len(l0)-c:
            l0=erase(l0,pos,"1")
        else:
            l0=erase(l0,pos,"0")
        pos+=1
    org=int("0b"+l0[0],base=2)

    #CO2 scrubber rating
    pos=0
    while len(l1)>1:
        c=countOnesPos(l1,pos)
        if c>len(l1)-c:
            l1=erase(l1,pos,"1")
        elif c<len(l1)-c:
            l1=erase(l1,pos,"0")
        else:
            l1=erase(l1,pos,"1")
        pos+=1
    csr=int("0b"+l1[0],base=2)
        
    return org*csr

def main():
    f=open('input.txt','r')
    #f=open('test.txt','r')
    inp=f.read()
    liste=inp.split("\n") #récupération de la liste dans le fichier
    #liste=[int(i) for i in liste] #conversion en int
    print("result:",calcDay3_2(liste))
    f.close()

main()
