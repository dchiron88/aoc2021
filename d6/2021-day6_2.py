# -*-coding:Latin-1 -*
import os

def nextDay(l):
    i=0
    lenth=len(l)
    while i<lenth:
        if l[i]>0:
            l[i]-=1
        else:
            l[i]=6
            l.append(8)
        i+=1
    return l

def calcDay6_1_old(l):
    #print(l)
    nbDays=80
    i=0
    while i<nbDays:
        l=nextDay(l)
        #print(l)
        i+=1
    return len(l)

def calcDay6(l,nbDays):
    #print(l)
    #nbDays=26984457539
    res=0
    for elt in l:
        created=f(elt,nbDays)
        print(elt,created)
        res+=created
    return res

#Function returning the nb of fish created giving initial internal timer (init) after d days
def f(init,d):
    #res=1+int((d-init+6)/7)+int((d-init-3))+int((d-init-10))+...
    res=1+int(max((d-init+6),0)/7)
    incr=3
    while d-init-incr>=7:
        res+=int(max((d-init-incr),0)/7)
        incr+=7
    return res

def main():
    #f=open('input.txt','r')
    f=open('test.txt','r')
    inp=f.read()
    liste=inp.split(",") #récupération de la liste dans le fichier
    liste=[int(i) for i in liste] #conversion en int
    print("result example:",calcDay6(liste,18))
    print("result part 1:",calcDay6(liste,80))
    print("result part 2:",calcDay6(liste,26984457539))
    f.close()

main()
