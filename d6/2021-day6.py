# -*-coding:Latin-1 -*
import os

def nextDay(l):
    i=0
    lenth=len(l)
    while i<lenth:
        if l[i]>0:
            l[i]-=1
        else:
            l[i]=6
            l.append(8)
        i+=1
    return l

def calcDay6_1(l):
    #print(l)
    nbDays=80
    i=0
    while i<nbDays:
        l=nextDay(l)
        #print(l)
        i+=1
    return len(l)

def calcDay6_2(l):
    #print(l)
    nbDays=26984457539
    i=0
    while i<nbDays:
        l=nextDay(l)
        #print(l)
        i+=1
    return len(l)

def main():
    f=open('input.txt','r')
    #f=open('test.txt','r')
    inp=f.read()
    liste=inp.split(",") #récupération de la liste dans le fichier
    liste=[int(i) for i in liste] #conversion en int
    print("result part 1:",calcDay6_1(liste))
    print("result part 2:",calcDay6_2(liste))
    f.close()

main()
