# -*-coding:Latin-1 -*
import os

def calcDay8_1(l):
    lEasyDigits=[2,4,3,7]
    nb=0
    for elt in l:
        a=elt.split(" | ")
        lt=a[1].split()
        for elt2 in lt:
            if len(elt2) in lEasyDigits:
                nb+=1
    return nb
    
def main():
    f=open('input.txt','r')
    #f=open('test.txt','r')
    inp=f.read()
    liste=inp.split("\n") #récupération de la liste dans le fichier
    #liste=[int(i) for i in liste] #conversion en int
    print("Result part 1 :",calcDay8_1(liste))
    f.close()

main()
